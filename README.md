ACTIVIDAD 1 
```plantuml
@startmindmap
caption Sandoval Caballero Josselyn Daniela 18161244
title MAPA CONCEPTUAL VON-NEUMAN

*[#Orange] Arquitectura de Von-Neumann 

**[#lightblue] Von-Neumann datos personales
***[#FFBBCC] Nació 28 diciembre 1903
***[#FFBBCC] Murió 8 de febrero 1953

**[#lightblue] Aportaciones en las ciencias computacionales 
***[#FFBBCC] 1945 desarrollo un programa de almacenamiento en memoria de los datos
***[#FFBBCC] Creo la arquitectura de los computadores 
***[#FFBBCC] 1944 Aportaciones en la computadora ENIAC

**[#lightblue] Aportaciones en matemáticas
***[#FFBBCC] Definición del número ordinal
***[#FFBBCC] Teoría de los conjuntos

**[#lightblue] Arquitectura de Computadoras
***[#FFBBCC] Se divide en cuatro segmentos

****[#lightgreen] Procesador 
*****[#lemonChiffon] Recibe información, realiza las operaciones y la devuelve
*****[#lemonChiffon] Se divide en dos unidades
******[#MistyRose] Unidad aritmético lógico
*******[#Plum] Realiza las operaciones del procesamiento de datos
******[#MistyRose] Unidad de control
*******[#Plum] Establece el tiempo del procesamiento

****[#lightgreen]  Almacenamiento 
*****[#lemonChiffon] Cualquier dispositivo debía tener la capacidad de guardar información
*****[#lemonChiffon] Existen dos categorías
******[#MistyRose]  Almacenamiento de largo plazo
*******[#Plum] Guarda infromación sin eliminarse o perderse 
********_  Disco Duro
********_  ROM
******[#MistyRose] Almacenamiento a corto tiempo
*******[#Plum] Es una memoria temporal mientras se este utilizando la computadora.
********_  RAM


****[#lightgreen]  Dispositivos de Entrada y Salida 
*****[#lemonChiffon] Tienen la finalidad de ingresar información al dispositivo y lo de salida saca la información
****[#lightgreen]  Buses 
*****[#lemonChiffon] Llevan y traen información por medio de pulsaciones eléctricas conocidas como bits
*****[#lemonChiffon] Se dividen en tres tipos Buses
******_  Datos
******_  Direcciones
******_  Control

@endmindmap

```
```plantuml
@startmindmap
caption Sandoval Caballero Josselyn Daniela 18161244
title MAPA CONCEPTUAL 
title  -SUPERCOMPUTADORAS EN MÉXICO-
*[#Orange] Supercomputadoras en México
**[#lightblue] Concepto
***[#FFBBCC] Una supercomputadora es una infraestructura que se diseña y se construye para procesar grandes cantidades de datos o información de una manera mucho mas rápida

**[#lightblue] Antecedentes 
***[#FFBBCC] Hace 51 años comenzó la era de la computación
***[#FFBBCC] 1958 La UNAM tuvo la primera computadora IBM 650
***[#FFBBCC] 80´s Se podian adquirir ya las computadoras en oficinas y en hogares

**[#lightblue] Supercomputadoras
***[#FFBBCC] 1991 La UNAM obtiene la Cray la primera super computadora
***[#FFBBCC] Kan Balam
****[#lightgreen] Utilizada para realizar proyectos de investigacion como la atronomía y la química cuántica
****[#lightgreen] Realiza siete millones de operaciones matemáticas por segundo

***[#FFBBCC] 2012 IPN crearon el laboratorio de cómputo de alto rendimiento llamado Xiuhcoat
****[#lightgreen] Tiene un tiempo de vida de cuatro años
****[#lightgreen] Posicionada en el segundo lugar como la computadora más rápida en Latinoamérica

***[#FFBBCC] 2017 Miztli la super computadora amplia su capacidad con un total de 8344 procesadores
****[#lightgreen] Realiza 120 proyectos de investigación anuales en la UNAM

***[#FFBBCC] 2018 La BUAP crea un laboratorio de computadoras  
****[#lightgreen]  Ocupando el quinto lugar como la computadora más rápida de América Latina
****[#lightgreen] Su aplicacion es para realizar simulaciones utilizada en aeronáutica y estudio automotriz
****[#lightgreen] En tan solo un segundo puede ejecutar hasta dos mil millones de operaciones

@endmindmap
```